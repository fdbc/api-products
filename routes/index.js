'use strict'

const express = require('express')
const productCtrl = require('../controlers/product')
const userCtrl = require('../controlers/user')
const auth = require('../middlewares/auth')
const api = express.Router()

api.get('/product', productCtrl.getProducts)
api.get('/product/:productId', productCtrl.getProduct)
api.post('/product',  auth , productCtrl.saveProduct)
api.put('/product/:productId', auth ,productCtrl.updateProduct)
api.delete('/product/:productId', auth ,productCtrl.deleteProduct)
api.post('/singup', userCtrl.singUp, (res, req) => {

})
api.post('/singin', userCtrl.singIn, (res, req) => {
    
})

module.exports = api